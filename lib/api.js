const request = require("request");
const rp = require('request-promise');
const BigNumber = require('bignumber.js');

const hosts = [
    'http://127.0.0.1:8283/api/v1',
    'https://blndaemon.blnscan.io/api/v1'
];
let host_index = 0;

module.exports = {

    getInfo: () => {
        return getApi('/ledger');
    },

    getNodeInfo: () => {
        return getApi('/nodeinfo');
    },

    getPeerDB: () => {
        return getApi('/peerdb');
    },

    getPeerInfo: () => {
        return getApi('/peerinfo');
    },

    getBlockHash: (height) => {

        return getApi(`/blockdb/getblockhash/${height}`);
    },

    getBlock: (hash, withTransactionOrNot) => {

        return getApi(`/blockdb/get/${hash}/${withTransactionOrNot}`);
    },

    getBalance: (address) => {

        return getApi(`/ledger/get/${address}`);
    },

    verifySign: (account, signature, message) => {
        return getApi("/verifymessage/" + account + "/" + signature + "/" + message + "/");
    },

    sign: (mnemonic, message) => {
        return postApi("/signmessage/" + mnemonic + "/" + message + "/");
    },

    transfer: (mnemonic, amount, to, message, encrypted) => {
        
        let fee = 100000;

        amount = new BigNumber(amount).times(1e8);

        return postApi("/transfer/" + mnemonic + "/" + fee + "/" + amount + "/" + to + "/" + message + "/" + encrypted + "/");
    },

    burn: (mnemonic, amount, message) => {
        
        let fee = 100000;

        amount = new BigNumber(amount).times(1e8);

        return postApi("/burn/" + mnemonic + "/" + fee + "/" + amount + "/" + Buffer.from(message).toString('hex') + "/");
    },
    lease: (mnemonic, amount, to) => {
        
        let fee = 100000;

        amount = new BigNumber(amount).times(1e8);

        return postApi("/lease/" + mnemonic + "/" + fee + "/" + amount + "/" + to + "/");
    },
    cancelLease	: (mnemonic, amount, to, height) => {
        
        let fee = 100000;

        amount = new BigNumber(amount).times(1e8);

        return postApi("/cancellease/" + mnemonic + "/" + fee + "/" + amount + "/" + to + "/"+ height + "/");
    },
    startStaking: (mnemonic) => {
        
        return postApi("/startStaking/" + mnemonic + "/");
    },
    stopStaking	: (mnemonic) => {
        
        return postApi("/stopStaking/" + mnemonic + "/");
    },
    decrypt	: (mnemonic,from,message) => {
        
        return postApi("/decryptmessage/" + mnemonic + "/"+ from + "/"+ message + "/");
    }
}

function getHost(){

    if(hosts[host_index]) return hosts[host_index];

    host_index = 0;

    return hosts[0];
}


function getApi(url) {
    
    return fetch(url);
}

function postApi(url) {
    
    let data, i = hosts.length;
    
    while(i-->0){
        data = process2(url);
        if(data === null) break;

        if(data.error == 'ECONNREFUSED'){
            host_index++;
            continue;
        }
    }
    return data;
}


async function fetch(url){
    
    let data, i = hosts.length;
    
    while(i-->0){
        data = await process(url);
        if(data === null) break;

        if(data.error == 'ECONNREFUSED'){
            host_index++;
            continue;
        }
    }
    return data;
}

function process2(url){

    let target_url = getHost() + url;
    return new Promise((resolve)=>{
        request({
            url: encodeURI(target_url),
            headers: {
                'User-Agent': 'Mozilla/5.0 (Linux; Android 8.0; Pixel 2 Build/OPD3.170816.012) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Mobile Safari/537.36'
            },
            forever: true,
            method: "POST",
            json: true
        }, (err, res, body)=>{

            if(err && err.code === 'ECONNREFUSED'){
                return resolve({error: 'ECONNREFUSED'});
            }
        
            if(err){
                console.log(err.stack);
                return resolve(null);
            }
            resolve(body);
        });
    });
}

function process(url){

    let target_url = getHost() + url;
    return new Promise((resolve)=>{
        request({
            url: encodeURI(target_url),
            headers: {
                'User-Agent': 'Mozilla/5.0 (Linux; Android 8.0; Pixel 2 Build/OPD3.170816.012) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Mobile Safari/537.36'
            },
            forever: true,
            json: true
        }, (err, res, body)=>{

            if(err && err.code === 'ECONNREFUSED'){
                return resolve({error: 'ECONNREFUSED'});
            }
        
            if(err){
                console.log(err.stack);
                return resolve(null);
            }
            resolve(body);
        });
    });
}
