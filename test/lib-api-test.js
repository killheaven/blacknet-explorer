const assert = require('assert'); // node自带的断言库

const API = require('../lib/api'); // 需要测试的模块

const should = require('should');

// 测试lib api模块
describe('#lib/api.js', () => {
    // getinfo方法
    describe('#getInfo()', () => {
        it('getInfo() should return object', async () => {
            await assert.doesNotReject(
                API.getInfo,
                SyntaxError
            );
        });
    });
    // getNodeInfo
    describe('#getNodeInfo()', () => {
        it('getNodeInfo() should return object', async () => {
            await assert.doesNotReject(
                API.getNodeInfo,
                SyntaxError
            );
        });
    });
    // getPeerDB
    describe('#getPeerDB()', () => {
        it('getPeerDB() should return object', async () => {
            await assert.doesNotReject(
                API.getPeerDB,
                SyntaxError
            );
        });
    });
    // getPeerInfo
    describe('#getPeerInfo()', () => {
        it('getPeerInfo() should return object', async () => {
            await assert.doesNotReject(
                API.getPeerInfo,
                SyntaxError
            );
        });
    });
    // getBlockHash
    describe('#getBlockHash()', () => {
        it('getBlockHash() should return blockhash', async () => {
            await assert.doesNotReject(
                async () => {
                    assert.strictEqual(await API.getBlockHash(1), "26507AD152777FB77848F3DCEABC67A949FCE0EAD9453E0997A3E16B8255E1FA");
                },
                SyntaxError
            );
        });
    });
    // getBlock
    describe('#getBlock()', () => {
        it('getBlock() should return object', async () => {
            await assert.doesNotReject(
                async () => {
                    assert.deepStrictEqual(await API.getBlock("26507AD152777FB77848F3DCEABC67A949FCE0EAD9453E0997A3E16B8255E1FA", true), { 
                        size: 173,
                        version: 0,
                        previous:
                         '0000000000000000000000000000000000000000000000000000000000000000',
                        time: 1545556624,
                        generator:
                         'blacknet1klnycx794hg9jvuhua0gy75d5v374rrwrlnpg25xpykfxkg30egqq83tj0',
                        contentHash:
                         '45B0CFC220CEEC5B7C1C62C4D4193D38E4EBA48E8815729CE75F9C0AB0E4C1C0',
                        signature:
                         '0BD14B678ED7C9C5E44E4C2EF6307416B44CFE3315D17345DAF80EF60CD684A5AABDFD0DA0983ED1EC8B3797E49D89053BE49FA2149597FB3E14AAA48DE02505',
                        transactions: [] 
                    })
                },
                SyntaxError
            );
        });
    });
    // getBalance
    describe('#getBalance()', () => {
        it('getBalance() should return object', async () => {
            await assert.doesNotReject(
                async () => {
                    assert.ok("account not found" === await API.getBalance("blacknet1wzpma9mtqezfh74jm6a9p93prjz7q9hggv76d0kw6qvezrzv0t5skgeyhc"))
                    // assert.deepStrictEqual(await API.getBalance("blacknet1klnycx794hg9jvuhua0gy75d5v374rrwrlnpg25xpykfxkg30egqq83tj0"), { seq: 6,
                    //     balance: 4660851760297513,
                    //     confirmedBalance: 4660851760297513,
                    //     stakingBalance: 4660851760297513 })
                },
                SyntaxError
            );
        });
    });
    // verifySign
    describe('#verifySign()', () => {
        it('verifySign() should return bool', async () => {
            await assert.doesNotReject(
                async () => {
                    assert.ok(await API.verifySign("blacknet1y36epd3xh7vt2yv2yu7pnk06amfpyqlcc2jlkf4caznmc4xlgqgqc6deuj", "0BBA9DE1D4FDE94CD427D7BF74FB2D5DC7F2C6007129D2EFE25033A38C1297F0BC6CFB4DEA3DCF981D575CC5733078DE65C3C2182A82634582C0330DD988130B", "test"))
                },
                SyntaxError
            );
        });
    });
    // Sign
    describe('#Sign()', () => {
        it('sign() should return signature string', async () => {
            await assert.doesNotReject(
                async () => {
                    assert.strictEqual(await API.sign("feature coyote burst swim saddle glance polar interest topple flock stomach one", "test"), "9FB6BDCB1614D46E039347B5817932ACDA2360F05F6EF1ECDEE40628A385283CA77FD20CEB7E4295490C0289620E7840CB04B279D029D46101F2200ADA203C0A")
                },
                SyntaxError
            );
        });
    });
    // Decrypt
    describe('#Decrypt()', () => {
        it('Decrypt() should return string', async () => {
            await assert.doesNotReject(
                async () => {
                    assert.notEqual(await API.decrypt("feature coyote burst swim saddle glance polar interest topple flock stomach one","blacknet1wzpma9mtqezfh74jm6a9p93prjz7q9hggv76d0kw6qvezrzv0t5skgeyhc", "9FB6BDCB1614D46E039347B5817932ACDA2360F05F6EF1ECDEE40628A385283CA77FD20CEB7E4295490C0289620E7840CB04B279D029D46101F2200ADA203C0A"), "Decryption failed");
                },
                SyntaxError
            );
        });
    });
    // Transfer
    describe('#Transfer()', () => {
        it('Transfer() should return txhash string', async () => {
            await assert.doesNotReject(
                async () => {
                    assert.strictEqual(await API.transfer("feature coyote burst swim saddle glance polar interest topple flock stomach one", 1, "blacknet1y36epd3xh7vt2yv2yu7pnk06amfpyqlcc2jlkf4caznmc4xlgqgqc6deuj", "test", "1"), "Transaction rejected")
                },
                SyntaxError
            );
        });
    });
    
    // Burn
    describe('#Burn()', () => {
        it('Burn() should return txhash string', async () => {
            await assert.doesNotReject(
                async () => {
                    assert.strictEqual(await API.burn("feature coyote burst swim saddle glance polar interest topple flock stomach one", 1, "test"), "Transaction rejected")
                },
                SyntaxError
            );
        });
    });
    // lease
    describe('#Lease()', () => {
        it('Lease() should return txhash string', async () => {
            await assert.doesNotReject(
                async () => {
                    assert.strictEqual(await API.lease("feature coyote burst swim saddle glance polar interest topple flock stomach one", 1, "blacknet1y36epd3xh7vt2yv2yu7pnk06amfpyqlcc2jlkf4caznmc4xlgqgqc6deuj"), "Transaction rejected")
                },
                SyntaxError
            );
        });
    });
    // cancelLease
    describe('#cancelLease()', () => {
        it('cancelLease() should return txhash string', async () => {
            await assert.doesNotReject(
                async () => {
                    assert.strictEqual(await API.cancelLease("feature coyote burst swim saddle glance polar interest topple flock stomach one", 1, "blacknet1y36epd3xh7vt2yv2yu7pnk06amfpyqlcc2jlkf4caznmc4xlgqgqc6deuj", 300000), "Transaction rejected")
                },
                SyntaxError
            );
        });
    });
    // startStaking
    describe('#startStaking()', () => {
        it('startStaking() should return bool', async () => {
            await assert.doesNotReject(
                async () => {
                    should(await API.startStaking("feature coyote burst swim saddle glance polar interest topple flock stomach one")).be.Boolean()
                },
                SyntaxError
            );
        });
    });
    // stopStaking
    describe('#stopStaking()', () => {
        it('stopStaking() should return bool', async () => {
            await assert.doesNotReject(
                async () => {
                    should(await API.stopStaking("feature coyote burst swim saddle glance polar interest topple flock stomach one")).be.Boolean()
                },
                SyntaxError
            );
        });
    });
});